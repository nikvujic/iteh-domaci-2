<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use \App\Models\User;

class RecipeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'=>$this->faker->word(),
            'cookTime'=>$this->faker->randomDigit(),
            'description'=>$this->faker->paragraph(),
            'dateAdded'=>now(),
            'user_id'=>User::factory(),
        ];
    }
}
