<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\User;
use \App\Models\MeasurementUnit;
use \App\Models\Ingredient;
use \App\Models\Recipe;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        Ingredient::truncate();
        Recipe::truncate();

        $users = User::factory(5)->create();  // create 5 users

        Ingredient::factory(15)->create();  // create 15 ingredients (also creates units for them)

        Recipe::factory(5)->create();  // create five recipes

        Recipe::factory(rand(2,5))->create([  // create 2-5 recipes belonging to a random user
            'user_id'=>$users->random(1)->pluck('id')[0]
        ]);

        $ings = Ingredient::all();

        Recipe::all()->each(function ($recipe) use ($ings) {  // populate the pivot table
            $ings_ids = $ings->random(rand(1,5))->pluck('id')->toArray();
            foreach ($ings_ids as $a) {
                $recipe->ingredients()->attach(
                    $a, ['quantity'=>rand(1,10)]
                );
            }
        });

        // $ing1 = Ingredient::create([
        //     'name'=>"Ingredient 1",
        //     'measurement_unit_id'=>MeasurementUnit::create()
        // ]);

        // $ing2 = Ingredient::create([
        //     'name'=>"Ingredient 2",
        //     'measurement_unit_id'=>MeasurementUnit::create()
        // ]);

        // $recipe = Recipe::create([
        //     'title'=>"Recipe 1",
        //     'cookTime'=>360,
        //     'description'=>'blahg.. bla',
        // ]);

        // $recipe->ingredients()->attach([$ing1->id, $ing2->id], ['quantity'=>5]);
    }
}
