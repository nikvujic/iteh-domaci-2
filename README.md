### Zahtevi za domaći zadatak

* Kreirati Laravel aplikaciju
* Kreirati najmanje 3 modela međusobno povezana (ne koristiti primer sa vežbi)
* Kreirati najmanje 4 migracije različite namena (kreiranje tabele, izmena tabele, ....)
* Popuniti bazu (mysql) podataka korišćenjem seeder-a
* Kreirati api rute i kontrolere za iste
* Verzije aplikacije pratiti pomoću alata za verzionisanje koda
* Kreirati REST API u okviru Laravel aplikacije
* REST API treba da vraća podatke u json formatu 
* API treba da sadrži najmanje 6 ruta
* API treba da sadrži najmanje 3 različita tipa ruta
* API mora da bude autentifikovan preko korisnika
