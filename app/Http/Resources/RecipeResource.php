<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\IngredientWithPivotCollection;

class RecipeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */

    public static $wrap = 'recipe';

    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'title' => $this->resource->title,
            'cookTime' => $this->resource->cookTime,
            'description' => $this->resource->description,
            'dateAdded' => $this->resource->dateAdded,
            'ingredients' => new IngredientWithPivotCollection($this->resource->ingredients)
        ];
    }
}
