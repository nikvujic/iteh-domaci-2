<?php

namespace App\Http\Controllers;

use App\Models\Recipe;
use App\Models\Ingredient;
use Illuminate\Http\Request;
use App\Http\Resources\RecipeResource;
use App\Http\Resources\RecipeCollection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recipes = Recipe::all();

        return new RecipeCollection($recipes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'cookTime' => 'required',
            'description' => 'required|string|max:600'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $recipe = Recipe::create([
            'title' => $request->title,
            'cookTime' => $request->cookTime,
            'description' => $request->description,
            'dateAdded' => now(),
            'user_id' => Auth::user()->id
        ]);

        $ingredientsQuantities = $request->ingredients;

        foreach($ingredientsQuantities as $ingQuant) {
            $ingredient = Ingredient::find($ingQuant['ingredient_id']);
            $recipe->ingredients()->attach(
                $ingredient, ['quantity'=>$ingQuant['quantity']]
            );
        }

        return response()->json(['status'=>'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipe)
    {
        return new RecipeResource($recipe);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recipe $recipe)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'cookTime' => 'required',
            'description' => 'required|string|max:600'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $recipe->title = $request->title;
        $recipe->cookTime = $request->cookTime;
        $recipe->description = $request->description;

        $new_ingredients = $request->ingredients;

        foreach ($recipe->ingredients as $existing_ing) {  // delete ingredients not provided in updated recipe
            $in_list = false;

            foreach ($new_ingredients as $new_ingredient) {
                if ($new_ingredient['ingredient_id'] == $existing_ing['id']) {
                    $in_list = true;
                }
            }

            if (!$in_list) {
                $recipe->ingredients()->detach($existing_ing);
            }
        }

        foreach ($new_ingredients as $new_ingredient) {
            if ($recipe->ingredients()->find($new_ingredient['ingredient_id'])) {
                $ingredient_to_update = $recipe->ingredients()->find($new_ingredient['ingredient_id']);
                $ingredient_to_update->pivot->quantity = $new_ingredient['quantity'];
                $ingredient_to_update->pivot->save();
            } else {
                $new_ing = Ingredient::find($new_ingredient['ingredient_id']);
                $recipe->ingredients()->attach($new_ing, ['quantity'=>$new_ingredient['quantity']]);
            }
        }

        return response()->json(['succes'=>'true']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe)
    {
        $recipe->delete();

        return response()->json(['succes'=>'true']);
    }
}
