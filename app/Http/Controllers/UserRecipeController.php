<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Recipe;
use App\Http\Resources\RecipeCollection;

class UserRecipeController extends Controller
{
    public function index($user_id) {
        $recipes = Recipe::get()->where('user_id', $user_id);

        return new RecipeCollection($recipes);
    }
}
