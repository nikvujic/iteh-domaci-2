<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Models\User;

class AuthController extends Controller
{
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string|max:255',
            'username'=>'required|string|max:255|unique:users',
            'password'=>'required|string|min:8'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $user = User::create([
            'name'=>$request->name,
            'username'=>$request->username,
            'password'=>Hash::make($request->password)
        ]);

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json(['data'=>$user,'access_token'=>$token,'token_type'=>'Bearer', ]);
    }

    public function login(Request $request) {
        if (!Auth::attempt($request->only('username','password'))) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }

        $user = User::where('username', $request['username'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json(['message'=>'Hi '.$user->name.', welcome to home','access_token'=>$token,'token_type'=>'Bearer', ]);
    }

    public function logout(Request $request) {
        Auth::user()->tokens->each(function($token, $key) {
            $token->delete();
        });

        return response()->json(["logout"=>"success"]);
    }
}
