<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'cookTime',
        'description',
        'dateAdded',
        'user_id'
    ];

    public function ingredients() {
        return $this->belongsToMany('\App\Models\Ingredient', 'recipes_ingredients')->withPivot('quantity');
    }

    public function user() {
        return $this->belongsTo('\App\Models\User');
    }
}
