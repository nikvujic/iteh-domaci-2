<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Ingredient extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'measurement_unit_id'
    ];


    public function measurementUnit() {
        return $this->belongsTo('\App\Models\MeasurementUnit');
    }
}
